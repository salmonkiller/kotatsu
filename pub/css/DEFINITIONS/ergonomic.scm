;;;-*-guile-*-scheme-*-;;;

(define body-bg "#aba")
(define site-name-color "#050")

(define nav-bg "#bcb")

(define header-bg "#aba")
(define hr-border "#898")

(define mid-bg "#bcb")

(define stack-bg "#bcb")
(define stack-border "#898")

(define table-border "#898")

(define th-bg "#898")
(define even-bg "#bcb")
(define odd-bg "#aba")

(define note-color "#3b3")
(define notice-color "#3bb")
(define public-color "#bb3")
(define news-color "#b33")


(define post-form-field-bg "#aba")
(define post-form-field-text "#000")
(define post-form-border "#898")
(define textbox-color "#000")
(define textbox-bg "#ddd")

(define post-bg "#aba")
(define post-border "#898")
(define post-highlight "#898")
(define indicator-color "#050")

(define text-color "#000")
(define new-link-color "#080")
(define old-link-color "#050")
(define name-color "#174")
(define capcode-color "#c00")
(define date-color "#030")
(define quote-color "#262")
(define shade "#888")


(set-output-file (%current-path) "../ergonomic.css")

(include-css (%current-path) "GENERIC.scm")
