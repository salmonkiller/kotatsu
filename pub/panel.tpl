<%= (fill-header rc (string-append "Admin Panel - " website-title) "<h2>Admin Panel</h2>" style admin #:class "infopage") %>

      <div class="stack" style="margin:5px">
        <b>Messages:</b>
        <br><br>
        <ul>
          <li><a href="/noticeboard">Noticeboard</a></li>
          <ul>
            
            <table style="width:100%"><tr>
                <th style="padding:0px">Notices</th> <th style="padding:0px">Private notes being shared with you</th>
              </tr><tr style="vertical-align:top"><td>
                  <%= (build-note-listing rc #:template "pub/note-short.tpl" #:type '("notice" "public") #:limit 3) %>
                </td><td style="padding-left:12px">
                  <%= (build-note-listing rc #:template "pub/note-short.tpl" #:type "note" #:shared #t #:links-target "noticeboard") %>
            </td></tr></table>

          </ul>
          <br>
          <li><a href="/notes-view">Personal Notes</a></li>
          <ul>
            <%= (build-note-listing rc #:template "pub/note-short.tpl" #:limit 3 #:personal #t) %>
          </ul>
          <br>
          <li><a href="/note-editor/new">New Note</a></li>
          <li><a href="/private-messages">PM Inbox</a></li>
        </ul>

        <br><br>
        
        <b>Aministration:</b> (work in progress)
        <br><br>
        <ul>
          <li>Report Queue</li>
          <li>Ban List</li>
          <li>Manage Users</li>
          <li>Moderation Log</li>
        </ul>
      </div>
      <p><b>Info:</b>
        To create News or a Noticeboard entries start by clicking <i>New Note</i>.<br>
        On the new note page you can select where you want the note to appear. No selection means it's a personal note only you can see.<br>
        You can also edit notes and turn existing ones into news items or noticeboard entries at a later time.<br>
      </p>

      <br><hr><br>
      
      <div style="display:table">
        <b>Change Password</b>
        <form enctype="multipart/form-data" method="post">
          <table class="postform" border="1" style="margin:5px">
            <tr>
              <td class="field">Name</td><td><input type="text" name="name" size="50"></td>
            </tr><tr>
              <td class="field">Current Password</td><td><input type="password" name="current-password" size="50"></td>
            </tr><tr>
              <td class="field">New Password</td><td><input type="password" name="new-password" size="50"></td>
            </tr><tr>
              <td class="field">Confirm Password</td><td><input type="password" name="confirm-password" size="50"></td>
            </tr><tr>
              <td class="field">Submit</td><td><input type="submit" name="submit" value="Submit"></td>
            </tr>
          </table>
        </form>
      </div>

<%= (fill-footer rc styles) %>
