<%= (fill-header rc (format #f "Links /~a/~a - ~a" path1 path2 website-title)
                    (format #f "<h2>Links from /~a/~a</h2>" path1 path2)
                    style admin #:class "links") %>

<h2>Posts: <%= path3 %><br>
linked from <%= (format #f "<a href=\"/~a/~a\">/~a/~a</a>" path1 path2 path1 path2) %></h2>

<%= (build-threads path1 path2 #:mode path3) %>

[<a href="<%= (format #f "/~a/~a" path1 path2) %> ">Return to thread</a>]

<%= (fill-footer rc styles) %>
