    <div class="thread">
      <input type="checkbox" name="posts" value=<%= (format #f "~a/~a/~a" path1 threadnum postnum) %> >【<%= threadnum %>】<a class="title" href=<%= (format #f "/~a/~a" path1 threadnum) %> > <%= subject %> </a><br><br>
      <b><a href=<%= (format #f "\"/~a/~a#1p\"" path1 threadnum) %>>1</a> <span id="1p" class="name"><%= name %></span> <span class="date"><%= date %></span><% (format #t (if image " <span class=\"imgops\">[<a href=\"https://imgops.com/http://~a/pub/img/~a/~a/~a\" target=\"_blank\">ImgOps</a>]</span>" "") (get-conf '(host name)) path1 threadnum image) %></b><br>
      <% (format #t (if image "File: <a title='~a' href=\"/pub/img/~a/~a/~a\">~a</a> (~aB)<br>" "") iname path1 threadnum image (shorten iname max-filename-display-length) size) %>
      <% (format #t (if image "<a target=\"_top\" href=\"/pub/img/~a/~a/~a\"><img class=\"OPimg\" src=\"/pub/img/~a/~a/~a\"></a>" "") path1 threadnum image path1 threadnum thumb) %>
      <blockquote><%= (truncate-comment comment max-comment-preview-lines mode) %></blockquote>
      <% (format #t (if (and (eq? mode 'preview) (> postcount (+ post-preview-count 1))) "<span class=\"shade\">~a posts omitted</span>" "") (- postcount post-preview-count 1)) %>

      <%= replies %>

      <br style="clear:both">
    </div>
    <br>
