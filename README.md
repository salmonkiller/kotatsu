# kotatsu

Hackable and easy to deploy imageboard software written in Guile Scheme.

## Dependencies

* guile
* artanis
* guile-dbi

## Installation

* Copy the Artanis engine config file `conf/artanis.conf.template` to `conf/artanis.conf` and edit it to your needs
* Copy the message board settings file `prv/modules/settings.scm.template` to `prv/modules/settings.scm` and edit to your needs
* Run the server with `./start-server`
