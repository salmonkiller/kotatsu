;;; TODO: use "btime" to insert into all posts as their creation time
;;;       but first we need to add a ctime to the post table
;;;       this means we need to learn how to do migrations properly?

;;;       Do not use persistent conn/mtable connections.
;;;       Re-create each time database access is needed

;;;       Use "connection pools"? This is automatic if db.enable = true
;;;       but we have it set to false. look at functions init-connection-pool and init-DB

;;;       Image files are not deleted when posts are deleted (or directories for threads)

;;;       In some cases variables are set 2 times, once on the ENTRY page and again in these functions.
;;;       This could be justification to just have functions for every page and forget about setting the environment on ENTRY

(define-module (modules imageboard)
  #:use-module (artanis artanis)
  #:use-module (artanis utils)
  #:use-module (artanis cookie)
  #:use-module (artanis config)

  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)

  #:use-module (modules settings)
  #:use-module (modules file-uploads) ; FIXME: is this needed in utils or imageboard.scm ?
  #:use-module (modules utils)
  #:use-module (modules migrations-sqlite3)

  #:export (get-database-connection ; FIXME: Remove this
            CM ; FIXME: Remove this
            
            fill-header
            fill-footer
            check-cooldown
            init-database
            get-news
            build-note-listing
            note-editor
            get-style
            set-style
            mod-login
            mod-logoff
            change-password
            get-password
            set-password
            get-session-id
            get-admin
            build-style-menu
            comment-filter

            print-thread
            ;build-thread-rows ; This can be removed now
            build-threads
            build-catalog
            post-note
            post-to-board
            post-to-thread
            mod-posts))

(set! *random-state* (random-state-from-platform))
(define CM #f)

(define (get-database-connection)
  (let* ((conn (connect-db 'sqlite3 (string-append db-name ".db")))
         (mtable (map-table-from-DB conn)))
    (cons conn mtable)))

(define* (fill-header rc pagetitle message style admin #:key (class "none"))
  (tpl->response (string-append "pub/" (if admin master-mod-header master-header) ".tpl") (the-environment)))

(define* (fill-footer rc styles)
  (let ((style-menu (build-style-menu styles)))
    (tpl->response (string-append "pub/" master-footer ".tpl") (the-environment))))

(define (check-cooldown ip)
  (let ((temp ((cdr CM) 'get 'cooldowns #:condition (where #:ip ip)))
        (ctime (time-second (current-time time-utc))))
    (if (null? temp)
      (begin
        ((cdr CM) 'set 'cooldowns #:ip ip #:tier1 ctime #:tier2 ctime #:counter 1)
        (cons #t ""))
      (let ((tier1 (assoc-ref (car temp) "tier1"))
            (tier2 (assoc-ref (car temp) "tier2"))
            (tier2-counter (assoc-ref (car temp) "counter")))
        (if (< ctime (+ tier2 tier2-cooldown))
          (if (>= tier2-counter tier2-post-limit)
            (cons #f (format #f "Error: You have made ~a posts in the last ~a seconds\nwhich is more than the configured maximum.\nPlease wait ~a more seconds and try posting again." tier2-counter tier2-cooldown (- (+ tier2 tier2-cooldown) ctime)))
            (if (< ctime (+ tier1 tier1-cooldown))
              (cons #f (format #f "Error: Post cooldown - Please wait ~a seconds and try again." (- (+ tier1 tier1-cooldown) ctime)))
              (begin
                ((cdr CM) 'set 'cooldowns (format #f "tier1=~a,counter=counter+1 where ip='~a'" ctime ip))
                (cons #t ""))))
          (if (< ctime (+ tier1 tier1-cooldown))
            (cons #f (format #f "Error: Post cooldown - Please wait ~a seconds and try again." (- (+ tier1 tier1-cooldown) ctime)))
            (begin
              ((cdr CM) 'set 'cooldowns (format #f "tier1=~a,tier2=~a,counter=1 where ip='~a'" ctime ctime ip))
              (cons #t ""))))))))

(define (init-database)
  (initialize-sqlite3-database (string-append db-name ".db") CM))

;(define* (build-news-listing template #:key (limit #f) (order 'desc))
;  (let ((news ((cdr CM) 'get 'notes #:order-by `(id ,order) #:ret limit #:condition (where #:type "news"))))
;    (string-join 
;      (map (lambda (item)
;             (format #f template (assoc-ref item "id")
;                                 (assoc-ref item "id")
;                                 (assoc-ref item "subject")
;                                 (assoc-ref item "name")
;                                 (assoc-ref item "date")
;                                 (replace (replace (assoc-ref item "body") "\\r\\n" "<br>") "\\n" "<br>")))
;           news)
;      "\n")))

;(define* (get-news rc #:key (limit #f) (order 'desc))
;  (let ((news-items ((cdr CM) 'get 'notes #:order-by `(id ,order) #:ret limit #:condition (where #:type "news"))))
;    (send-html (tpl->response "pub/news.tpl")

(define* (build-note-listing rc #:key (id #f) (limit #f) (type "note") (order 'desc) (template "pub/note.tpl") (personal #f) (shared #f) (links-target #f))
  (let* ((admin (get-admin rc))
         (notes
          (if id
            ((cdr CM) 'get 'notes #:condition (where #:id id))
            (if personal
              ((cdr CM) 'get 'notes #:order-by `(ctime ,order) #:ret limit #:condition (where #:name (assoc-ref (car admin) "name")))
              (let ((full ((cdr CM) 'get 'notes #:order-by `(ctime ,order) #:ret limit #:condition (where #:type type))))
                (if (not shared)
                  full
                  (remove (lambda (x)
                            (not (member (assoc-ref (car admin) "name") (string-split (assoc-ref x "read") #\space))))
                          full)))))))
    (string-join 
     (map (lambda (note)
            (let ((id (assoc-ref note "id"))
                  (type (assoc-ref note "type"))
                  (subject (assoc-ref note "subject"))
                  (name (assoc-ref note "name"))
                  (edited (assoc-ref note "edited"))
                  (date (assoc-ref note "date"))
                  (body (assoc-ref note "body"))
                  (links-target (if links-target links-target
                                  (case (string->symbol (if (list? type) (car type) type))
                                    ((note) "notes-view")
                                    ((notice) "noticeboard")
                                    ((public) "noticeboard")
                                    ((news) "news")))))
              (tpl->response template (the-environment))))
              ;(format #f template id id subject name date (replace (replace body "\\r\\n" "<br>") "\\n" "<br>") subject (replace (replace body "\\r\\n" "\n") "\\n" "\n"))))
          notes)
     "\n")))

(define (note-editor rc id)
  (let* ((cookies (get-cookie-alist rc))
         (style (car (get-style rc cookies)))
         (admin (get-admin rc cookies))
         (note ((cdr CM) 'get 'notes #:condition (where #:id id))))
    (if (equal? id "new")
      (let ((type "note")
            (editable #t)
            (name (assoc-ref (car admin) "name"))
            (perms-read '())
            (perms-write '()))
        (send-html (tpl->response "pub/note-editor.tpl" (the-environment))))
      (let* ((name (assoc-ref (car note) "name"))
             (type (assoc-ref (car note) "type"))
             (perms-read (string-split (assoc-ref (car note) "read") #\space))
             (perms-write (string-split (assoc-ref (car note) "write") #\space))
             (readable (and admin
                            (or (member type '("notice" "public" "news"))
                                (equal? name (assoc-ref (car admin) "name")) ;owner
                                (member (assoc-ref (car admin) "name") perms-read))))
             (editable (and admin
                            (or (equal? type "public")
                                (equal? name (assoc-ref (car admin) "name")) ;owner
                                (member (assoc-ref (car admin) "name") perms-write))))
             (subject (assoc-ref (car note) "subject"))
             (date (assoc-ref (car note) "date"))
             (body (assoc-ref (car note) "body"))
             (links-target (case (string->symbol (if (list? type) (car type) type))
                             ((note) "notes-view")
                             ((notice) "noticeboard")
                             ((public) "noticeboard")
                             ((news) "news"))))
        (if readable
          (send-html (tpl->response "pub/note-editor.tpl" (the-environment)))
          (throw 'artanis-err 401 post-note "Permission Denied."))))))

(define (get-style rc cookies) ;; FIXME: This should not be needed
  (let* ((cookie-style (assoc-ref cookies "style")))
    (if cookie-style
      (let ((style (car cookie-style)))
        (if (member style styles)
          (cons style (delete style styles))
          styles))
      styles)))

(define (set-style rc)
  (let* ((data (bv->alist (rc-body rc)))
         (style (default (assoc-ref data 'style) "default"))
         (request ((record-accessor (record-type-descriptor rc) 'request) rc))
         (headers ((record-accessor (record-type-descriptor request) 'headers) request))
         (referer (assoc-ref headers 'referer))
         (scheme ((record-accessor (record-type-descriptor referer) 'scheme) referer)))
    (rc-set-cookie! rc `(,(new-cookie #:npv `(("style" . ,style)) #:expires 315360000 #:http-only #f)))
    ;(:cookies-set! rc 'cc "style" style)
    ;(:cookies-setattr! rc 'cc #:expires 315360000 #:path "/" #:secure #f #:http-only #f)
    ;(:cookies-update! rc) ;; FIXME: Documentation says this isn't needed, but it seems to be
    (redirect-to rc (uri-path referer) #:scheme scheme)))

(define (mod-login rc)
  (let* ((ip (get-ip rc))
         (cooldown (check-cooldown (get-ip rc))))
    (if (not (car cooldown))
      (let ((message (cdr cooldown)))
        (send-html (tpl->response "pub/message.tpl" (the-environment))))

      (let* ((data (bv->alist (rc-body rc)))
             (name (assoc-ref data 'name))
             (password (assoc-ref data 'password))
             (mod ((cdr CM) 'get 'mods #:columns '(name) #:condition (where #:name name #:password password))))
        (if (null? mod)
          (let ((message "Error: Invalid login."))
            (send-html (tpl->response "pub/message.tpl" (the-environment))))
          (begin
            (let ((new-key (random-string 60)))
              ((cdr CM) 'set 'mods (format #f "session=\"~a\" where name=\"~a\"" new-key name))
              (rc-set-cookie! rc `(,(new-cookie #:npv `(("admin" . ,new-key)) #:expires 315360000 #:http-only #f)))
              ;(:cookies-set! rc 'cc "admin" "1")
              ;(:cookies-setattr! rc 'cc #:expires 315360000 #:path "/" #:secure #f #:http-only #f)
              ;(:cookies-update! rc) ;; FIXME: Documentation says this isn't needed, but it seems to be
              
              ;; -----------------------------------------------
              ;; FIXME: These aren't needed except to get scheme
              (let* ((request ((record-accessor (record-type-descriptor rc) 'request) rc))
                     (headers ((record-accessor (record-type-descriptor request) 'headers) request))
                     (referer (assoc-ref headers 'referer))
                     (scheme ((record-accessor (record-type-descriptor referer) 'scheme) referer)))
                ;; -----------------------------------------------
                ;; -----------------------------------------------
                (redirect-to rc "/panel" #:scheme scheme)))))))))

(define (mod-logoff rc)
  (let* ((cookies (get-cookie-alist rc))
         (key (assoc-ref cookies "admin")))
    (when key
      ((cdr CM) 'set 'mods (format #f "session=\"0\" where session=\"~a\"" key)))
    (rc-set-cookie! rc `(,(new-cookie #:npv `(("admin" . "0")) #:expires 0 #:http-only #f)))))

(define (change-password rc)
  (let* ((data (bv->alist (rc-body rc)))
         (name (assoc-ref data 'name))
         (current-password (assoc-ref data 'current-password))
         (new-password (assoc-ref data 'new-password))
         (confirm-password (assoc-ref data 'confirm-password)))
    (if (not (equal? new-password confirm-password))
      (let ((message "Error: New password doesn't match confirmation field"))
        (send-html (tpl->response "pub/message.tpl" (the-environment))))
      (let ((mod ((cdr CM) 'get 'mods #:columns '(password) #:condition (where #:name name #:password current-password))))
        (if (null? mod)
          (let ((message (format #f "Error: Invalid Credentials: ~a : ~a" name current-password)))
            (send-html (tpl->response "pub/message.tpl" (the-environment))))
          (begin
            ((cdr CM) 'set 'mods (format #f "password=\"~a\" where name=\"~a\"" new-password name))
            (let ((message "Password updated successfully."))
              (send-html (tpl->response "pub/message.tpl" (the-environment))))))))))

(define* (get-password rc #:optional cookies)
  (let* ((cookies (if cookies
                      cookies (get-cookie-alist rc)))
         (pass (assoc-ref cookies "password")))
    (if pass (car pass) "")))

(define (set-password rc newpass)
  (let ((oldpass (get-password rc)))
    (when (not (equal? newpass oldpass))
      (rc-set-cookie! rc `(,(new-cookie #:npv `(("password" . ,newpass)) #:expires 315360000 #:http-only #f))))))
      ;(:cookies-set! rc 'cc "password" newpass)
      ;(:cookies-setattr! rc 'cc #:expires 315360000 #:path "/" #:secure #f #:http-only #f)
      ;(:cookies-update! rc)))) ;; FIXME: Documentation says this isn't needed, but it seems to be

(define* (get-session-id rc #:optional cookies) ; FIXME: perhaps parent functions can pass IP into this function, instead of using get-ip?
  (let ((pass (get-password rc (if cookies cookies (get-cookie-alist rc)))))
    (if (equal? pass "")
      (get-ip rc)
      pass)))

(define* (get-admin rc #:optional cookies)
  (let* ((cookies (if cookies
                      cookies (get-cookie-alist rc)))
         (admin (assoc-ref cookies "admin")))
    ;  ;; FIXME: very inelegant
    (if (and admin
             (not (equal? "0" (car admin))))
      (let ((mod ((cdr CM) 'get 'mods #:condition (where #:session (car admin)))))
        (if (null? mod)
          #f
          mod))
      #f)))

(define (build-style-menu styles)
  (string-join
    (map (lambda (n style)
           (string-append "<option value=\"" style "\">" style "</option>" (if (= n 0) "<option disabled>----------</option>" "")))
         (iota (length styles))
         styles)
    "\n"))

(define (post-string->list str)
  (define (range->list rng i)
    (let* ((a (string->number (substring rng 0 i)))
           (b (string->number (substring rng (+ i 1)))))
      (map number->string (iota (- b a -1) a 1))))

  (let loop ((lst (string-split str #\,)))
    (if (null? lst)
      '()
      (let ((i (string-contains (car lst) "-")))
        `(,@(if i (range->list (car lst) i)
                  `(,(car lst)))
          ,@(loop (cdr lst)))))))

(define (comment-filter comment board thread)
  (fold (lambda (filt str)
          (filt str))
        (eliminate-evil-HTML-entities comment)
        (list
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          ;; BEGIN LIST OF FILTER FUNCTIONS ;;
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          (lambda (p)
            (regexp-substitute/global #f "\r\n" p
              'pre "\n" 'post))
          (lambda (p)
            (regexp-substitute/global #f "(http|https|ftp)://[^\n ]+" p
              'pre (lambda (m) (string-append "<a target=_top href=" (match:substring m) ">" (match:substring m) "</a>")) 'post)) ; FIXME : add quotes around the href and target once you figure out how to escape chars properly
          (lambda (p)
            (regexp-substitute/global #f "&gt;&gt;&gt;[a-zA-Z0-9/,-\\#]*[a-zA-Z0-9]" p
              'pre
              (lambda (m)
                (let* ((post-string (substring (match:substring m) 12))
                       (post-list (string-split post-string #\/)))
                  (if (equal? "" (car post-list))
                    (cond
                      ((= 3 (length post-list))
                       (let ((brd (cadr post-list))
                             (trd (caddr post-list)))
                         (if (and (assoc-ref boards brd)
                                  (string->number trd))
                           (string-append "<a href=/" brd "/" trd ">" (match:substring m) "</a>")
                           (match:substring m))))
                      ((= 4 (length post-list))
                       (let* ((brd (cadr post-list))
                              (trd (caddr post-list))
                              (psts (cadddr post-list))
                              (lnks (post-string->list psts)))
                         (if (and (assoc-ref boards brd)
                                  (string->number trd))
                           (string-append "<a href=/" brd "/" trd
                                          (if (= (length lnks) 1)
                                            (string-append "#" psts "p")
                                            (string-append "/" psts))
                                          ">" (match:substring m) "</a>")
                           (match:substring m))))
                      (else (match:substring m)))
                    (cond
                      ((= 1 (length post-list))
                       (string-append "<a href=/" post-string ">" (match:substring m) "</a>"))
                      (else (match:substring m))))))
              'post))
          (lambda (p)
            (regexp-substitute/global #f "&gt;&gt;[0-9/,\\-]*[0-9]" p
              ;; FIXME : ids are currently have a p suffix, this needs to be a p prefix, but there is some difficulty with the templating
              'pre
              (lambda (m)
                (let* ((post-string (substring (match:substring m) 8))
                       (post-list (string-split post-string #\/)))
                  (if (equal? "" (car post-list))
                    (match:substring m)
                    (cond
                      ((= 2 (length post-list))
                       (let* ((trd (car post-list))
                              (psts (cadr post-list))
                              (lnks (post-string->list psts)))
                         (if (string->number trd)
                           (string-append "<a href=/" board "/" trd
                                          (if (= (length lnks) 1)
                                            (string-append "#" psts "p")
                                            (string-append "/" psts))
                                          ">" (match:substring m) "</a>")
                           (match:substring m))))
                      ((= 1 (length post-list))
                       (let* ((psts (car post-list))
                              (lnks (post-string->list psts)))
                         (string-append "<a href=/" board "/" thread
                                        (if (= (length lnks) 1)
                                          (string-append "#" psts "p")
                                          (string-append "/" psts))
                                        ">" (match:substring m) "</a>")))
                      (else (match:substring m))))))
              'post)) ; FIXME : add quotes around the href once you figure out how to escape chars properly
          (lambda (p)
            (regexp-substitute/global #f "(^|\n)&gt;[^\n]+" p
              'pre (lambda (m) (string-append "<i>" (match:substring m) "</i>")) 'post))
          (lambda (p)
            (regexp-substitute/global #f "\\[spoiler\\].*\\[/spoiler\\]" p
              'pre (lambda (m) (string-append "<span class=\\\"spoiler\\\">" (substring (match:substring m) 9 (- (string-length (match:substring m)) 10)) "</span>")) 'post))
          (lambda (p)
            (regexp-substitute/global #f "\\[aa\\].*\\[/aa\\]" p
              'pre (lambda (m) (string-append "<span class='aa'>" (substring (match:substring m) 4 (- (string-length (match:substring m)) 5)) "</span>")) 'post))
          (lambda (p)
            (regexp-substitute/global #f "\n" p
              'pre "<br>" 'post)))))
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
          ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Available modes: 'default 'preview (list of post links)
(define* (build-threads path1 path2 #:key (mode 'normal) (OP-tpl post-OP-template) (post-tpl post-template) (page 1))
  (define* (build-post threadnum post #:optional postcount subject replies)
    (let ((postnum (assoc-ref post "postnum"))
          (name (assoc-ref post "name"))
          (date (assoc-ref post "date"))
          (image (assoc-ref post "image"))
          (thumb (assoc-ref post "thumb"))
          (iname (assoc-ref post "iname"))
          (size (assoc-ref post "size"))
          (comment (replace (replace (assoc-ref post "comment") "\\u3000" "　") "\\\\" "\\"))) ; FIXME: why are these necessary? Bashslashes are not properly converted when reading from database? May need to send bug report. But this works for now.
      (tpl->response (string-append "pub/" (if (or (not replies) (string? mode))
                                             post-tpl OP-tpl)
                                    ".tpl") (the-environment))))

  (string-join
      (let* ((page-len (assq-ref (assoc-ref boards path1) 'page-len))
             (listing (if (eq? mode 'preview)
                        ((cdr CM) 'get 'threads #:condition (where (format #f "board='~a' order by btime desc limit ~a offset ~a" path1 page-len (* (if (and page (string->number page))
                                                                                                                                                      (- (string->number page) 1)
                                                                                                                                                      0)
                                                                                                                                                    page-len))))
                        ((cdr CM) 'get 'threads #:order-by '(btime desc) #:condition (where #:threadnum path2 #:board path1)))))
        (map (lambda (thread)
               (let* ((threadnum (assoc-ref thread "threadnum"))
                      ;(subject (cdaar ((cdr CM) 'get 'threads #:columns '(subject) #:condition (where #:id threadnum))))
                      (subject (assoc-ref thread "subject"))
                      (postcount (assoc-ref thread "postcount"))
                      ;(table (if (eq? mode 'preview)
                      ;         ((cdr CM) 'get (string->symbol threadname) #:order-by '(id asc) #:condition (where (format #f "id=1 or id>~a-~a" maxid post-preview-count)))
                      ;         ((cdr CM) 'get (string->symbol threadname) #:order-by '(id asc))))
                      (posts (case mode
                               ((normal)  ((cdr CM) 'get 'posts #:order-by '(postnum asc) #:condition (where (format #f "board='~a' and threadnum=~a" path1 threadnum))))
                               ;((preview) ((cdr CM) 'get 'posts #:order-by '(postnum asc) #:condition (where (format #f "board='~a' and threadnum=~a and (postnum=1 or postnum>~a-~a)" path1 threadnum postcount post-preview-count))))
                               ((preview) (DB-query (car CM) (format #f "SELECT * FROM (SELECT * FROM posts WHERE board='~a' AND threadnum=~a AND postnum=1) UNION SELECT * FROM (SELECT * FROM posts WHERE board='~a' AND threadnum=~a ORDER BY POSTNUM DESC LIMIT ~a);" path1 threadnum path1 threadnum post-preview-count))
                                          (DB-get-all-rows (car CM)))
                               ;(else       ((cdr CM) 'get (string->symbol threadname) #:order-by '(id asc)))))
                               (else      ((cdr CM) 'get 'posts #:order-by '(id asc) #:condition (where (format #f "board='~a' and threadnum=~a and (postnum in (~a))" path1 threadnum (string-join (post-string->list mode) ", ")))))))
                      (replies (string-join (map (lambda (post)
                                                   (build-post threadnum post))
                                                 (cdr posts))
                                            "\n")))
                 (build-post threadnum (car posts) postcount subject replies)))
                 ;(format #f "table: ~a" table)))
             listing))
    "\n"))

(define* (build-catalog path1 #:key (catalog-tpl catalog-thread-template))
  (define* (build-post threadnum post #:optional postcount subject)
    (let ((image (assoc-ref post "image"))
          (thumb (assoc-ref post "thumb"))
          (comment (replace (replace (assoc-ref post "comment") "\\u3000" "　") "\\\\" "\\"))) ; FIXME: why are these necessary? Bashslashes are not properly converted when reading from database? May need to send bug report. But this works for now.
      (tpl->response (string-append "pub/catalog-thread.tpl") (the-environment))))

  (string-join
      (let ((listing ((cdr CM) 'get 'threads #:order-by '(btime desc) #:condition (where #:board path1))))
        (map (lambda (thread)
               (let* ((threadnum (assoc-ref thread "threadnum"))
                      (subject (assoc-ref thread "subject"))
                      (postcount (assoc-ref thread "postcount"))
                      (posts ((cdr CM) 'get 'posts #:condition (where (format #f "board='~a' and threadnum=~a and postnum=1" path1 threadnum)))))
                 (build-post threadnum (car posts) postcount subject)))
                 ;(format #f "table: ~a" table)))
             listing))
    "\n"))

;(define (build-thread-rows path1 template)
;  (let ((board (assoc-ref boards path1)))
;    (string-join
;      (let ((listing ((cdr CM) 'get 'threads #:order-by '(ctime desc) #:condition (where #:board path1))))
;        (map (lambda (n thread)
;               ;(print-thread path1 (assoc-ref thread "id") #t))
;               (format #f template (if (even? n) "even" "odd") (assoc-ref thread "id") path1 (assoc-ref thread "id") (assoc-ref thread "subject") (assoc-ref thread "date") (assoc-ref thread "replies")))
;               ;; FIXME: use "date" here instead of ctime, we just used ctime temporarily because the date column didn't exist
;               (iota (length listing))
;               listing))
;      "<br>")))

;(define (post-news rc)
;  (let ((admin (get-admin rc)))
;    (if admin
;      (let* ((data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
;                               (rc-body rc)))
;             (subject (assoc-ref data 'subject))
;             (date (date->string (current-date 0) "~5"))
;             (ctime (time-second (current-time time-utc)))
;             (body (assoc-ref data 'body)))
;        ((cdr CM) 'set 'notes #:type "news" #:name (assoc-ref (car admin) "name") #:read "" #:write "" #:subject subject #:ctime ctime #:date date #:body body)
;
;        ;; -----------------------------------------------
;        ;; FIXME: These aren't needed except to get scheme
;        (let* ((request ((record-accessor (record-type-descriptor rc) 'request) rc))
;              (headers ((record-accessor (record-type-descriptor request) 'headers) request))
;              (referer (assoc-ref headers 'referer))
;              (scheme ((record-accessor (record-type-descriptor referer) 'scheme) referer)))
;        ;; -----------------------------------------------
;        ;; -----------------------------------------------
;          (redirect-to rc "/news-post" #:scheme scheme)))
;
;      (throw 'artanis-err 401 post-news "Not Admin."))))

(define (post-note rc)
  (let ((admin (get-admin rc)))
    (if (not admin)
      (throw 'artanis-err 401 post-note "Unauthorized.")

      (let* ((id (params rc "path2"))
             (data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
                               (rc-body rc)))
             (name (assoc-ref (car admin) "name"))
             (subject (assoc-ref data 'subject))
             (date (date->string (current-date 0) "~5"))
             (ctime (time-second (current-time time-utc)))
             (body (assoc-ref data 'body))
             (type (assoc-ref data 'type))
             (perms-read (string-join (default (get-all-alist-keys data 'perm-read) '()) " "))
             (perms-write (string-join (default (get-all-alist-keys data 'perm-write) '()) " "))
             (del (string-join (default (get-all-alist-keys data 'delete) '()) " "))
             ;; -----------------------------------------------
             ;; FIXME: These aren't needed except to get the scheme
             (request ((record-accessor (record-type-descriptor rc) 'request) rc))
             (headers ((record-accessor (record-type-descriptor request) 'headers) request))
             (referer (assoc-ref headers 'referer))
             (scheme ((record-accessor (record-type-descriptor referer) 'scheme) referer)))
             ;; -----------------------------------------------
             ;; -----------------------------------------------
        (if (equal? id "new")
          (begin
            ((cdr CM) 'set 'notes #:type type #:name name #:read perms-read #:write perms-write #:subject subject #:ctime ctime #:btime ctime #:date date #:edited "" #:body body)
            (redirect-to rc "/panel" #:scheme scheme))


          (let* ((note ((cdr CM) 'get 'notes #:condition (where #:id id)))
                 (creator (assoc-ref (car note) "name"))
                 (init-type (assoc-ref (car note) "type"))
                 (init-perms-write (string-split (assoc-ref (car note) "write") #\space))
                 (editable (and admin
                                (or (equal? init-type "public")
                                    (equal? creator name) ;owner
                                    (member name init-perms-write)))))
            (if editable
              (begin
                (if (equal? del "delete")
                  (DB-query (car CM) (format #f "delete from notes where id=~a;" id))
                  ((cdr CM) 'set 'notes #:type type #:read perms-read #:write perms-write #:subject subject #:btime ctime #:edited (format #f "~a on ~a" name date) #:body body (where #:id id)))
                (redirect-to rc "/panel" #:scheme scheme))
              (throw 'artanis-err 401 post-note "Unauthorized."))))))))

(define (post-to-board rc)
  (let* ((ip (get-ip rc))
         (cooldown (check-cooldown (get-ip rc))))
    (if (not (car cooldown))
      (let ((message (cdr cooldown)))
        (send-html (tpl->response "pub/message.tpl" (the-environment))))

      (let* ((board (params rc "path1"))
             (data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
                               (rc-body rc)))
             (cookies (get-cookie-alist rc))
             (admin (get-admin rc cookies)) ; FIXME: is this needed if there's a built-in function to get cookie alist?
             (subject (eliminate-evil-HTML-entities (default (assoc-ref data 'subject) default-subject)))
             (name (let* ((name (eliminate-evil-HTML-entities (default (assoc-ref data 'name) (assq-ref (assoc-ref boards board) 'name))))
                          (cc (string-contains name " ## ")))
                     (if (and admin
                              cc (>= cc 1)
                              ;(equal? name "!capcode"))
                              (equal? (assoc-ref (car admin) "name")
                                      (substring name 0 cc)))
                       (string-append "<span class=capcode>" (assoc-ref (car admin) "name") " ## " (substring name (+ cc 4)) "</span> <img src=/pub/img/capcode.png>")
                       name)))
             (options (default (assoc-ref data 'options) ""))
             (pass (default (assoc-ref data 'password) ""))
             ;(date (date->string (current-date) "(~k:~M) ~a ~b ~e, ~Y"))
             (date (date->string (current-date 0) "~5"))
             (ctime (time-second (current-time time-utc)))
             (threadnum (let ((val ((cdr CM) 'get 'boards #:columns '(threadcount) #:condition (where #:board board))))
                          (if (null? val)
                            1
                            (+ 1 (assoc-ref (car val) "threadcount")))))
             (comment (default (comment-filter (assoc-ref data 'comment) board (number->string threadnum)) default-comment))
             (tname (string->symbol (string-append "thread" (number->string threadnum)))))
    
        ;; FIXME: There's probably a better way to do this
        (let ((file-info (store-uploaded-files rc #:path (string-append (getcwd) "/pub/img/upload")
                                               #:uid #f
                                               #:gid #f
                                               #:simple-ret? #f
                                               #:mode #o664
                                               #:path-mode #o775
                                               #:sync #f)))
          (let* ((filename (caaddr file-info))
                 (fsize (caadr file-info))
                 (extension (let ((lst (string-split filename #\.)))
                              (if (> (length lst) 1)
                                (list-ref lst (- (length lst) 1))
                                #f)))
                 (fname (if (equal? filename "")
                          (cons #f #f)
                          (let* ((newboarddir (string-append (getcwd) "/pub/img/" board))
                                 (newthreaddir (string-append newboarddir "/" (number->string threadnum)))
                                 (timestamp (get-timestamp13))
                                 (newname (string-append (number->string timestamp) (if extension (string-append "." extension) "")))
                                 (newthumbname (string-append (number->string timestamp) (if extension (string-append "s." extension) "s")))
                                 (newfile (string-append newthreaddir "/" newname))
                                 (newthumb (string-append newthreaddir "/" newthumbname)))
                            (unless (file-exists? newboarddir) (mkdir newboarddir))
                            (unless (file-exists? newthreaddir) (mkdir newthreaddir))
                            (rename-file (string-append (getcwd) "/pub/img/upload/" filename) newfile)
                            (system (string-append "convert " newfile " -resize 250x250 " newthumb))
                            (cons newname newthumbname)))))

            (cond
              ((check-string-limits subject max-name-length #:length-error "Error: Subject length too long"
                                                            #:lines-error "Error: Subject has too many lines") => identity)
              ((check-string-limits name max-name-length #:length-error "Error: Name length too long"
                                                         #:lines-error "Error: Name has too many lines") => identity)
              ((check-string-limits comment max-comment-length #:max-lines max-comment-lines
                                                               #:linebreak "<br>"
                                                               #:length-error "Error: Comment is too long"
                                                               #:lines-error "Error: Comment has too many lines") => identity)
              ((check-string-limits filename max-filename-length #:length-error "Error: Filename length too long"
                                                                 #:lines-error "Error: Filename has too many lines") => identity)
              (else

                (let ((sage (string-contains options "sage")))
                  ((cdr CM) 'set 'posts #:board board #:threadnum threadnum #:postnum 1 #:ip ip #:name (if sage (string-append "<a href=mailto:sage>" name "</a>") name)
                                     #:date date #:ctime ctime #:image (car fname) #:thumb (cdr fname) #:iname filename #:size (number->string fsize) #:comment comment))

                (set-password rc pass)
                ((cdr CM) 'set 'threads #:board board #:threadnum threadnum #:postcount 1 #:subject subject #:date date #:ctime ctime #:btime ctime)
                (if (= threadnum 1)
                  ((cdr CM) 'set 'boards #:board board #:threadcount 1)
                  ((cdr CM) 'set 'boards (format #f "threadcount=~a where board='~a'" threadnum board)))

                ;; -----------------------------------------------
                ;; FIXME: These aren't needed except to get the scheme
                (let* ((request ((record-accessor (record-type-descriptor rc) 'request) rc))
                      (headers ((record-accessor (record-type-descriptor request) 'headers) request))
                      (referer (assoc-ref headers 'referer))
                      (scheme ((record-accessor (record-type-descriptor referer) 'scheme) referer)))
                ;; -----------------------------------------------
                ;; -----------------------------------------------
                  (redirect-to rc (string-append "/" board "/"
                                                 (number->string threadnum))
                               #:scheme scheme))))))))))

(define (post-to-thread rc)
  (let* ((ip (get-ip rc))
         (cooldown (check-cooldown (get-ip rc))))
    (if (not (car cooldown))
      (let ((message (cdr cooldown)))
        (send-html (tpl->response "pub/message.tpl" (the-environment))))

      (let* ((board (params rc "path1"))
             (threadnum (params rc "path2"))
             (ip (get-ip rc))
             (data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
                               (rc-body rc)))
             (cookies (get-cookie-alist rc))
             (admin (get-admin rc cookies)) ; FIXME: is this needed if there's a built-in function to get cookie alist?
             (name (let* ((name (eliminate-evil-HTML-entities (default (assoc-ref data 'name) (assq-ref (assoc-ref boards board) 'name))))
                          (cc (string-contains name " ## ")))
                     (if (and admin
                              cc (>= cc 1)
                              ;(equal? name "!capcode"))
                              (equal? (assoc-ref (car admin) "name")
                                      (substring name 0 cc)))
                       (string-append "<span class=capcode>" (assoc-ref (car admin) "name") " ## " (substring name (+ cc 4)) "</span> <img src=/pub/img/capcode.png>")
                       name)))
             (options (default (assoc-ref data 'options) ""))
             (pass (default (assoc-ref data 'password) ""))
             ;(date (date->string (current-date) "(~k:~M) ~a ~b ~e, ~Y"))
             (date (date->string (current-date 0) "~5"))
             (ctime (time-second (current-time time-utc)))
             (comment (default (comment-filter (assoc-ref data 'comment) board threadnum) default-comment))
             (thread (car ((cdr CM) 'get 'threads #:columns '(postcount ctime btime) #:condition (where (format #f "board='~a' and threadnum=~a" board threadnum)))))
             (postnum (+ 1 (assoc-ref thread "postcount")))
             (btime (assoc-ref thread "btime"))
             ;(postnum (+ 1 (assoc-ref (car ((cdr CM) 'get 'threads #:columns '(postcount ctime) #:condition (where (format #f "board='~a' and threadnum=~a" board threadnum)))) "postcount")))
             )

        ;; FIXME: There's probably a better way to do this
        (set-password rc pass)
        (let ((file-info (store-uploaded-files rc #:path (string-append (getcwd) "/pub/img/upload")
                                               #:uid #f
                                               #:gid #f
                                               #:simple-ret? #f
                                               #:mode #o664
                                               #:path-mode #o775
                                               #:sync #f)))
          ;(format #t "\n\n!!!!! [~a]\n\n" (null? (caddr file-info)))
          (let* ((filename (if (null? (caddr file-info)) "" (caaddr file-info)))
                 (fsize (if (null? (cadr file-info)) 0 (caadr file-info)))
                 (extension (let ((lst (string-split filename #\.)))
                              (if (> (length lst) 1)
                                (list-ref lst (- (length lst) 1))
                                #f)))
                 (fname (if (equal? filename "")
                          (cons #f #f)
                          (let* ((newboarddir (string-append (getcwd) "/pub/img/" board))
                                 (newthreaddir (string-append newboarddir "/" threadnum))
                                 (timestamp (get-timestamp13))
                                 (newname (string-append (number->string timestamp) (if extension (string-append "." extension) "")))
                                 (newthumbname (string-append (number->string timestamp) (if extension (string-append "s." extension) "s")))
                                 (newfile (string-append newthreaddir "/" newname))
                                 (newthumb (string-append newthreaddir "/" newthumbname)))
                            (unless (file-exists? newboarddir) (mkdir newboarddir))
                            (unless (file-exists? newthreaddir) (mkdir newthreaddir))
                            (rename-file (string-append (getcwd) "/pub/img/upload/" filename) newfile)
                            (system (string-append "convert " newfile " -resize 150x150 " newthumb))
                            (cons newname newthumbname)))))

            (cond
              ((check-string-limits name max-name-length #:length-error "Error: Name length too long"
                                                         #:lines-error "Error: Name has too many lines") => identity)
              ((check-string-limits comment max-comment-length #:max-lines max-comment-lines
                                                               #:linebreak "<br>"
                                                               #:length-error "Error: Comment is too long"
                                                               #:lines-error "Error: Comment has too many lines") => identity)
              ((check-string-limits filename max-filename-length #:length-error "Error: Filename length too long"
                                                                 #:lines-error "Error: Filename has too many lines") => identity)
              (else

                (let ((sage (string-contains options "sage")))
                  ((cdr CM) 'set 'posts #:board board #:threadnum threadnum #:postnum postnum #:ip ip #:name (if sage (string-append "<a href=mailto:sage>" name "</a>") name)
                                     #:date date #:ctime ctime #:image (car fname) #:thumb (cdr fname) #:iname filename #:size (number->string fsize) #:comment comment)
                  ;((cdr CM) 'set 'threads (string-append "btime=" (number->string ctime) ",replies=replies+1 where id=" (params rc "path2")))
                  ((cdr CM) 'set 'threads (format #f "btime=~a,postcount=~a where board='~a' and threadnum=~a" (if sage btime ctime) postnum board threadnum)))

                ;; -----------------------------------------------
                ;; FIXME: These aren't needed except to get scheme
                (let* ((request ((record-accessor (record-type-descriptor rc) 'request) rc))
                      (headers ((record-accessor (record-type-descriptor request) 'headers) request))
                      (referer (assoc-ref headers 'referer))
                      (scheme ((record-accessor (record-type-descriptor referer) 'scheme) referer)))
                ;; -----------------------------------------------
                ;; -----------------------------------------------
                  (redirect-to rc (string-append "/" board "/" threadnum)
                               #:scheme scheme))))))))))

(define (mod-posts rc)
  (let* ((ip (get-ip rc))
         (data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
                           (rc-body rc)))
         (cookies (get-cookie-alist rc))
         (admin (get-admin rc cookies)) ; FIXME: is this needed if there's a built-in function to get cookie alist?
         (delete-button (assoc-ref data 'delete-button))
         (modaction (if delete-button 'del
                        (string->symbol (default (assoc-ref data 'modaction) ""))))
         (posts (default (get-all-alist-keys data 'posts) '())))
    (case modaction
      ((del)
       (let ((statuses
              (fold ;;; FIXME: This queries the database once per post in the list, this is too much and could be used to slow down the server
                (lambda (post status)
                  (let ((lst (string-split post #\/)))
                    (if (= (length lst) 3)
                      (let* ((board (car lst))
                             (threadnum (cadr lst))
                             (postnum (caddr lst))
                             ;(table ((cdr CM) 'get 'posts #:columns '(ctime) #:condition (where (format #f "board='~a' and threadnum=~a and postnum=~a and ip='~a'" board threadnum postnum ip))))
                             (table ((cdr CM) 'get 'posts #:condition (where (format #f "board='~a' and threadnum=~a and postnum=~a and ip='~a'" board threadnum postnum ip))))
                             (ctime (time-second (current-time time-utc))))
                        (if (or admin
                                (and (not (null? ctime))
                                     (< (- ctime (assoc-ref (car table) "ctime")) post-deletion-period)))
                          (begin
                            (if (equal? postnum "1")
                              (begin
                                (DB-query (car CM) (format #f "delete from posts where board='~a' and threadnum=~a;" board threadnum))
                                (DB-query (car CM) (format #f "delete from threads where board='~a' and threadnum=~a;" board threadnum)))
                              (DB-query (car CM) (format #f "delete from posts where board='~a' and threadnum=~a and postnum=~a;" board threadnum postnum)))
                            (cons (string-append post ": <span style=\"color:#0c0\">SUCCESS</span>") status))
                          (cons (string-append post ": <span style=\"color:#c00\">FAILED</span> - TIME EXPIRED, OR PERMISSION DENIED") status)))
                      (cons (string-append post ": <span style=\"color:#c00\">FAILED</span> - BAD FORMAT") status)))) ; FIXME: This uses the raw db connection, there should be a way to do this with mtable instead : NOTE: The reason why is because WHEN you change this to delete ALL posts with a single query (instead of 1-at-a-time) you will need it like this.
                '()
                posts)))
         (let ((message (format #f "Post Deletions:<br>~a" (string-join statuses "<br>"))))
           (send-html (tpl->response "pub/message.tpl" (the-environment))))))
      (else
       (let ((message "Mod command not understood."))
         (send-html (tpl->response "pub/message.tpl" (the-environment))))))))
