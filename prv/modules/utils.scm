(define-module (modules utils)
  #:use-module (artanis artanis)
  #:use-module (artanis utils)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 iconv)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)

  #:export (check-string-limits
            truncate-comment
            get-all-alist-keys
            random-string
            shorten
            get-ip
            get-timestamp13
            get-timestamp10
            send-html
            ;send-error
            get-cookie-alist
            default
            read-file
            bv->alist
            split-form
            replace))

(define* (check-string-limits str max-length #:key (max-lines 1) (linebreak "\n") (length-error "Error: String length too long") (lines-error "Error: Too many lines"))
  (let ((len (string-length str))
        (lin (length (string-split2 str linebreak))))
    (cond
      ((> len max-length)
       (let ((message (format #f "~a (~a/~a)" length-error len max-length)))
         (send-html (tpl->response "pub/message.tpl" (the-environment)))))
      ((> lin max-lines)
       (let ((message (format #f "~a (~a/~a)" lines-error lin max-lines)))
         (send-html (tpl->response "pub/message.tpl" (the-environment)))))
      (else
       #f))))

(define (truncate-comment comment max-lines mode)
  (case mode
    ((normal) comment)
    ((preview) (let ((csplit (string-split2 comment "<br>")))
                 (if (> (length csplit) max-lines)
                   (string-append (string-join (take csplit max-lines)
                                               "<br>")
                                  "<br><span class=\"shade\">Comment too long. View thread to read entire comment.</span>")
                   comment)))
    (else comment)))

(define (string-split2 str del)
  (let loop ((str2 str))
    (let ((idx (string-contains str2 del)))
      (if idx
        (cons (substring str2 0 idx)
              (loop (substring str2 (+ idx (string-length del)))))
        (list str2)))))

(define (get-all-alist-keys alist key)
  (let next-val ((lst alist))
    (if (null? lst)
      '()
      (if (equal? (caar lst) key)
        (cons (eliminate-evil-HTML-entities (cdar lst)) (next-val (cdr lst)))
        (next-val (cdr lst))))))

(define (random-string len)
  (let ((pool "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"))
    (list->string (list-tabulate len (lambda (x) (string-ref pool (random 62)))))))

(define (shorten str max-len)
  (if (> (string-length str) max-len)
    (string-append (substring str 0 max-len) "…")
    str))

(define (get-ip rc) ; FIXME: we have a lot of functions accessing request and headers, can we simplify it so it only occurs 1 time per request?
  (let* ((request ((record-accessor (record-type-descriptor rc) 'request) rc))
         (headers ((record-accessor (record-type-descriptor request) 'headers) request))
         (ip (assoc-ref headers 'x-forwarded-for))
         (ip-cf (assoc-ref headers 'cf-connecting-ip)))
    (cond
      (ip-cf ip-cf)
      (ip ip)
      (else "none"))))

(define (get-timestamp13)
  (let ((ctime (current-time time-utc)))
    (inexact->exact
      (floor
        (+ (* (time-second ctime) 1000.)
           (/ (time-nanosecond ctime) 1000000.))))))
(define (get-timestamp10)
  (let ((ctime (current-time time-utc)))
    (inexact->exact
      (floor
        (+ (* (time-second ctime) 1000.)
           (/ (time-nanosecond ctime) 1000000.))))))

(define* (send-html response #:optional (status 200))
  (let* ((response-length (bytevector-length (string->bytevector response "utf8"))))
    (response-emit response #:status status #:headers `((content-type . (text/html))
                                                     (content-length . ,response-length)))))

;(define (send-error error-message env)
;  (let ((error-message error-message))
;    (send-html (tpl->response "pub/error.tpl" (the-environment)))))

(define (get-cookie-alist rc) ; FIXME: This gets run 2 times
  (let ((cookie-lst (rc-cookie rc)))
    (if (null? cookie-lst)
      '()
      (let* ((cookies (car cookie-lst))
             (nvps ((record-accessor (record-type-descriptor cookies) 'nvp) cookies)))
        (if (null? nvps)
          '()
          nvps)))))

(define (default option def)
  (if (or (null? option)
          (eq? option #f)
          (equal? option ""))
    def
    option))

(define (read-file filename)
  (with-input-from-file filename
    (lambda ()
      (let loop ((ls1 '()) (c (read-char)))
        (if (eof-object? c)
          (list->string (reverse ls1))
          (loop (cons c ls1) (read-char)))))))

(define (bv->alist bv)
  (if (not bv)
    '()
    (let ((str (utf8->string bv))
          (head "\r\nContent-Disposition: form-data; name=\""))
      ;(format #t "\n\nBYTEVECTOR-STRING=[~a]\n\n" str)
      (let ((test (string-contains str head)))
        (if (not test)
          (begin (display "ERROR READING FORM DATA\n") '())
          (split-form str (substring str 0 test) (string-append (substring str 0 test) head)))))))

(define (split-form form-str token separator)
  (let loop ((str form-str)
             (idx (string-length token)))
    (if (not (string-prefix? separator str))
      '()
      (let* ((key (substring str (string-length separator) (string-index str #\" (string-length separator))))
             (end (string-contains str token idx)))
        (cons (cons (string->symbol key)
                    (substring str (+ (string-length separator) (string-length key) 5) (- end 2)))
              (loop (substring str end)
                    idx))))))

(define (replace str old new) ; FIXME: This function is shit, and shouldn't be needed anyway
  (let loop ((str str)
             (idx 0))
    (let ((jdx (string-contains str old idx)))
      (if jdx
        (loop (string-append
                (substring str 0 jdx)
                new
                (substring str (+ jdx (string-length old)) (string-length str)))
              (+ jdx (string-length new)))
        str))))
