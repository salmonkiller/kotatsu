#! /run/current-system/profile/bin/guile
;;;-*-guile-scheme-*-;;; !#

(define-module (modules settings)) ; Leave this alone

(define-public db-name "db/kotatsu") ; FIXME: This should not be necessary

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Edit the settings below ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-public website-title "Kotatsu Channel")
(define-public greeting "<br>Greeting message goes here<br><br>")
(define-public default-subject "Untitled") ; Default new thread subject if left blank by user
(define-public default-comment "ｷﾀ━━━(ﾟ∀ﾟ)━━━!!") ; Default comment if left blank by user
(define-public post-preview-count 5) ; Amount of posts to display under a thread when viewing from a board page

;;; Cooldowns - A 2 tier system is good for bot spam
(define-public tier1-cooldown 15) ; Tier 1: A flat cooldown (in seconds) on all posts. Set to 0 to disable Tier 1
(define-public tier2-post-limit 50) ; Tier 2: Maximum amount of posts that can be made within
(define-public tier2-cooldown 3600) ;         <-- this period of time (seconds). Set tier2-cooldown to 0 to disable Tier 2

(define-public post-deletion-period 3600) ; In seconds

;;; Maximum field lengths
(define-public max-subject-length 60)
(define-public max-name-length 40)
(define-public max-comment-length 4000)
(define-public max-comment-lines 80)
(define-public max-comment-preview-lines 40) ; longer posts will be truncated on board preview, but displayed in full within threads
(define-public max-filename-length 200)
(define-public max-filename-display-length 30) ; filenames will be shortened, but full filename will be displayed as tooltip
;(define-public max-session-length 40)

;;; Main template files (will look inside pub/ for a file with this name and .tpl extension)
;;; NOTE: The purpose of this is to make it easy to switch templates
;;;       (e.g. switch to holiday themed templates, you can even add scheme code to check the date and apply templates accordingly)
(define-public master-header "master-header") ; Template that shows up at the top of most pages
(define-public master-mod-header "master-mod") ; Header template for mods 
(define-public master-footer "master-footer") ; Template for footer that shows up at the bottom of pages
(define-public post-OP-template "post-OP") ; Template for thread OP's
(define-public post-template "post") ; Template for normal posts
(define-public catalog-thread-template "catalog-thread") ; Template for normal posts

;;; Enable/Disable stylesheets (stylesheet files will be searched in pub/css/ with extension .css)
(define-public styles '("default" "yotsuba" "yotsubab" "ergonomic"))

;;; Define boards here
;;; NOTE: New boards added to this list are automatically added to the database, however removing boards which have threads on them will not remove them from the database, but will make them innaccessible to users
(define-public boards `(("a"    . ((title    . "Anime")
                                   (name     . "Nameless")
                                   (images   . #t)
                                   (page-len . 15)
                                   (pages    . 10)
                                   (template . "board")))
                        ("prog" . ((title    . "Computers & Programming")
                                   (name     . "Nameless")
                                   (images   . #t)
                                   (page-len . 15)
                                   (pages    . 10)
                                   (template . "board")))
                        ("ck"   . ((title    . "Cooking")
                                   (name     . "Nameless")
                                   (images   . #t)
                                   (page-len . 15)
                                   (pages    . 10)
                                   (template . "board")))
                        ("jp"   . ((title    . "Otaku Culture")
                                   (name     . "Nameless")
                                   (images   . #t)
                                   (page-len . 15)
                                   (pages    . 10)
                                   (template . "board")))
                        ("b"    . ((title    . "Random")
                                   (name     . "Nameless")
                                   (images   . #t)
                                   (page-len . 15)
                                   (pages    . 10)
                                   (template . "board")))))

;;; Define mods:
(define-public mods `(; NAME      PERMISSIONS  DEFAULT_PASSWORD(this field is only used until the mod changes their password)
                      ("Admin"   "*"          "abc123")
                      ("SomeMod" "*"          "abc123")))

(define-public admin-only-pages '("panel" "note-editor" "notes-view" "logoff")) ; These are pages which should throw and "unauthorized" error if accessed by non-moderators
