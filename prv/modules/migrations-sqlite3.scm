#! /run/current-system/profile/bin/guile
;;;-*-guile-scheme-*-;;; !#


;;; Remove srfi-1 and srfi-19 dependency after deleting temporary upgrade 0-1
;;; remove (modules utils)
;;; Remove function time-13-to-10



(define-module (modules migrations-sqlite3)
  #:use-module (artanis artanis)
  #:use-module (modules settings)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (modules utils)
  #:export (initialize-sqlite3-database))


(define (initialize-sqlite3-database db-path CM)
    (migrate db-path CM)
    (migrate-mods CM))






(define (time-ensure-10-dig num)
  (let ((numstr (number->string num)))
    (if (<= (string-length numstr) 10)
      num
      (string->number (substring numstr 0 10)))))





(define latest-database-version 1)

(define (new-database db-path CM)
  (format #t "Initializing new database ~a ...\n" db-path)
  (((cdr CM) 'create 'cooldowns '((ip text)
                                  (tier1 integer)
                                  (tier2 integer)
                                  (counter integer))
    #:if-exists? 'ignore #:engine #f)
   'valid?)
  (((cdr CM) 'create 'boards '((id integer (#:primary-key))
                               (board text)
                               (threadcount integer))
    #:if-exists? 'ignore #:engine #f)
   'valid?)
  (((cdr CM) 'create 'threads '((id integer (#:primary-key))
                                (board text)
                                (threadnum integer)
                                (postcount integer)
                                (subject text)
                                (date text)
                                (ctime integer)
                                (btime integer))
    #:if-exists? 'ignore #:engine #f)
   'valid?)
  (((cdr CM) 'create 'posts '((id integer (#:primary-key))
                              (board text)
                              (threadnum integer)
                              (postnum integer)
                              (ip text)
                              (session text)
                              (name text)
                              (date text)
                              (ctime integer)
                              (image text)
                              (thumb text)
                              (iname text)
                              (size text)
                              (comment text))
    #:if-exists? 'ignore #:engine #f)
   'valid?)
  (((cdr CM) 'create 'mods '((id integer (#:primary-key))
                             (session text)
                             (ctime integer)
                             (name text)
                             (permissions text)
                             (password text))
    #:if-exists? 'ignore #:engine #f)
   'valid?)
  (((cdr CM) 'create 'notes '((id integer (#:primary-key))
                              (type text) ; Type is note, news, notice, or public. Notice = note with * read permissions, public = note with * write permissions
                              (name text)
                              (read text)
                              (write text)
                              (subject text)
                              (ctime integer)
                              (btime integer)
                              (date text)
                              (edited text)
                              (body text))
    #:if-exists? 'ignore #:engine #f)
   'valid?)

  (DB-query (car CM) (format #f "PRAGMA user_version=~a;" latest-database-version)))

(define (migrate db-path CM)
  (DB-query (car CM) (format #f "PRAGMA user_version;"))
  (let ((version (cdar (DB-get-top-row (car CM)))))
    (format #t "Database Version: ~a\n" version)
    (case version
      ((0) (new-database db-path CM))
      ((1) (format #t "Database ~a is latest version.\n" db-path)))))

(define (migrate-mods CM)
  ;(format #t "Mod table up to date.\n"))
  (let ((existing-mods (map cdar ((cdr CM) 'get 'mods #:columns '(name)))))
    (for-each (lambda (mod)
                (unless (member (car mod) existing-mods)
                  ((cdr CM) 'set 'mods #:session "none" #:ctime (get-timestamp13) #:name (car mod) #:permissions (cadr mod) #:password (caddr mod))))
              mods)))
